import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import 'hammerjs';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarslistComponent} from './carslist/carslist.component';
import { SetMaterialModule} from './material-module';
import { platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MyfavoritesComponent } from './myfavorites/myfavorites.component';

@NgModule({
  declarations: [
    AppComponent,
    CarslistComponent,
    MyfavoritesComponent,
    MyfavoritesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SetMaterialModule,
    FormsModule,
    ReactiveFormsModule

  ],
  entryComponents: [MyfavoritesComponent, CarslistComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
platformBrowserDynamic().bootstrapModule(AppModule);
