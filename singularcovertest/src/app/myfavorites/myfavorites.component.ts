import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource} from '@angular/material';
import { DialogData, Cars } from '../carslist/carslist.component';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-myfavorites',
  templateUrl: './myfavorites.component.html',
  styleUrls: ['./myfavorites.component.css']
})
export class MyfavoritesComponent implements OnInit {
datosFavoritos: Observable<any>;
dataSource = new MatTableDataSource<Cars>();
  constructor(
    public dialogRef: MatDialogRef<MyfavoritesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.data.datos);
  }
  removeFavorite(id) {
    const index = this.data.datos.map(e => e.id).indexOf(id);
    this.data.datos.splice(index, 1);
    this.dataSource._updateChangeSubscription();
  }
  applyFilterFavorites(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
