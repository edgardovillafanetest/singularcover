import { Component, OnInit, ViewChild, Inject} from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, startWith} from 'rxjs/operators';
import { FormControl} from '@angular/forms';
import { MatDialog} from '@angular/material';
import { environment } from '../../environments/environment';
import { MyfavoritesComponent } from '../myfavorites/myfavorites.component';

export interface Cars {
  id: number;
  image: string;
  brand: string;
  model: string;
  price: number;
}
export interface DialogData {
  datos: any;
  displayedColumns: string[];
  pathImages: string;
}
@Component({
  selector: 'app-carslist',
  templateUrl: './carslist.component.html',
  styleUrls: ['./carslist.component.css']
})

export class CarslistComponent implements OnInit {
  // brands
  filterByBrand = new FormControl();
  brands: string[] = [];
  filteredBrands: Observable<string[]>;
  favoriteCars: any[] = [];
  minPrice: number;
  maxPrice: number;
  displayedColumns: string[] = ['image', 'brand', 'model', 'price', 'favorite'];
  displayedFavorites: string[] = ['image', 'brand', 'model', 'favorite'];
  dataSource = new MatTableDataSource<Cars>();
  origin = new MatTableDataSource<Cars>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public pathImages: string;
  constructor(
    private http: HttpClient,
    public dialog: MatDialog) {
    this.getJSON().subscribe(data => {
      // subscribe to observable CARS from json File
        this.dataSource.data = data;
        this.origin.data = data;
      // get min and max price in database
      this.getMinMaxPrice(data);
      // observer changes in filter box
      this.filteredBrands = this.filterByBrand.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(value))
        );

      // observer changes in prices slider


    });
    // environment var to images folder
    this.pathImages = environment.pathImages;
}
public getJSON(): Observable<any> {
  // environment var to jSon file
  return this.http.get(environment.jSonSourceFile);
}
  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.brands.filter(brand => brand.toLowerCase().includes(filterValue));
  }

  openModal(): void {
    // open Modal and send favorites data object to Modal
    const dialogRef = this.dialog.open(MyfavoritesComponent, {
      width: '550px',
      data: {
        datos: this.favoriteCars,
        displayedColumns: this.displayedFavorites,
        pathImages: this.pathImages
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  addFavorite(id) {
    const selectedCar = this.dataSource.data.find(o => o.id === id);
   // search if exists in favorites array
    if (!this.favoriteCars.find(o => o.id === id)) {
      // add the selected car to favorites array
       this.favoriteCars.push(selectedCar);
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    // set the paginator to first page
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }
    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }
    return value;
  }

  getMinMaxPrice(data) {
    // get the min and max price from database
    this.minPrice = Math.min.apply(null, data.map(item => item.price)),
    this.maxPrice = Math.max.apply(null, data.map(item => item.price));
  }

  filterPrice(event: any) {
    // filter the database by including cars with price less than filter query
    this.dataSource.data = this.origin.data.filter(o => o.price <= event.value);
  }
}


